This app packages FileRun <upstream>2017.01.23</upstream>.

[FileRun](http://www.filerun.com) is an elegant file manager which doubles as a self-hosted Google Drive alternative.

## Features

### Share your largest files

* Don't be limited by email. With FileRun, you can share even the largest files with a simple link.

* Create a secure link to any file that can be shared with anyone via email.

* Share entire folders with others, so they can not only see all your content, but also upload their own.

### Send file requests

* File requests allow you to collect and receive files from anyone, right into your FileRun user account, with just a link.

* File requests are ideal for receiving a large file or collection of files, collecting photos after a special event, and requesting submissions from coworkers and clients.


### No import required

* Just like you do with an FTP server, point FileRun to where you keep the files on your server and you will get instant web access to them.

* Want to access your files also by FTP, SMB or WebDAV (server included)? No problem. Upload, download or manage the same files.

* All changes made to the files using FTP or other method reflect via FileRun without delay.

### Your files everywhere

* Access your files from wherever you are using the free mobile apps.

* Sync files from your desktop using the free desktop apps.

* Access via WebDAV. FileRun comes with its own embedded WebDAV server. No server configuration needed.


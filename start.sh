#!/bin/bash
set -eu

mkdir -p /run/filerun/sessions

if [ ! -e /app/data/html/index.php ];  then
    mkdir -p /app/data/html
    mkdir -p /app/data/user-files/superuser
    unzip /filerun.zip -d /app/data/html
    cp /app/config.php /app/data/html/customizables/config.php
    cp /app/logo.png /app/data/user-files/superuser/Welcome.png
    rm /app/data/html/system/classes/vendor/FileRun/Utils/DB.php
    cp /app/DB.php /app/data/html/system/classes/vendor/FileRun/Utils/DB.php
    mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} < /app/db.sql
fi  

chown -R www-data:www-data /app/data /run /tmp

#needs to run on every start as the connection info might change
sed -e "s/##MYSQL_DATABASE/${CLOUDRON_MYSQL_DATABASE}/" \
    -e "s/##MYSQL_USERNAME/${CLOUDRON_MYSQL_USERNAME}/" \
    -e "s/##MYSQL_PASSWORD/${CLOUDRON_MYSQL_PASSWORD}/" \
    -e "s/##MYSQL_HOST/${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}/" \
    /app/autoconfig.template > /app/data/html/system/data/autoconfig.php # sed -i seems to destroy symlink

# start
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/html

RUN apt-get update && apt-get install -y ghostscript libgs-dev ffmpeg && rm -r /var/cache/apt /var/lib/apt/lists

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf

RUN a2disconf other-vhosts-access-log
ADD apache/filerun.conf /etc/apache2/sites-enabled/filerun.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

# ioncube. the extension dir comes from php -i | grep extension_dir
# extension has to appear first, otherwise will error with "The Loader must appear as the first entry in the php.ini file"
# ioncube does not seem to have support for 8.0 yet
RUN mkdir /tmp/ioncube && \
    curl http://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz | tar zxvf - -C /tmp/ioncube && \
    cp /tmp/ioncube/ioncube/ioncube_loader_lin_7.4.so /usr/lib/php/20190902/ && \
    rm -rf /tmp/ioncube && \
    echo "zend_extension=/usr/lib/php/20190902/ioncube_loader_lin_7.4.so" > /etc/php/7.4/apache2/conf.d/00-ioncube.ini && \
    echo "zend_extension=/usr/lib/php/20190902/ioncube_loader_lin_7.4.so" > /etc/php/7.4/cli/conf.d/00-ioncube.ini

# Install filerun
RUN curl -o /filerun.zip -L https://www.filerun.com/download-latest

# set recommended PHP.ini settings
# see http://docs.filerun.com/php_configuration
ADD filerun-optimization.ini /etc/php/7.0/apache2/conf.d/filerun-optimization.ini

ADD db.sql start.sh DB.php config.php autoconfig.template logo.png /app/
RUN chown -R www-data.www-data /app/html

CMD [ "/app/start.sh" ]
